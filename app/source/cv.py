def Lev_Barsov:
    '''
    Address: Kiryat Bialik, ISRAEL
    Phone:   052-4794480
    e-Mail:  lbarsov@gmail.com
    Origin:  www.lbarsov.site
    '''

    PERSONAL_DETAILS = {
        Born: "February 24, 1967, Alma-Ata, USSR",
        Marital_status: "Divorced",
        Arrived_in_Israel:  "November 27, 1997",
        Education: "M.A., Applied Mathematics",
        Languages: [ 'English', 'Hebrew', 'Russian'.native(True) ]
    }

    SUMMARY = '''
        Near 30 years of varied experience as a programmer, researcher, and instructor in
        the field of development information systems. I have rich experience in independent & team
        working. I'm strong C++ programmer in kernel level programming (RT services, drivers,
        protocols for Linux & Windows), but also have big experience to work in any programming
        environment & programming languages (see experience list).

        I have wide practical experience in construction of distributed information and control and
        management systems. I know and successfully use calculus methods as well as methodology
        of optimization and reorganization of artificial systems.

        In recent years, I have been working in the field of web development which includes web apps
        and browser extensions.
    '''

    EXPERIENCE = {
        '2014 – present': {
                position: "Web Developer",
                company: "Sergata.NET Ltd.".location("Haifa"),
                description: '''
                    Web development: web/intranet/mobile apps, browser extensions, etc.
                ''',
                professional_skills: [
                    ('NodeJS','AngularJS','Bootstrap','Foundation','Bourbon'),
                    ('C#','.NET','MVC','WebAPI'), ('MSSQL','NoSQL'),
                    ('HTML5','SASS','CSS3'), ('XML','SVG','RDF'), ('SOAP','RPC')
                ]
            },
        '2009 – 2014': {
                position: "Freelancer",
                description: '''
                    Web development: representative sites, CMS, web shops, e-currency exchange, etc.
                ''',
                professional_skills: [
                    ('Python','PHP'), ('MySQL','PgSQL','NoSQL'), ('JQuery','AJAX','JSON'),
                    ('XML','RDF'), ('SOAP','RPC')
                ]
            },
        '2007 – 2009': {
                position: "System Developer",
                company: "Celtro Ltd.".location("Petah-Tikwa"),
                description: '''
                    Development of the Cellular Network Management System for Windows platform.
                ''',
                professional_skills: [
                    ('C++','MFC','Win32'), ('TCP/IP protocols', 'cellular network protocols')
                ]
            },
        '2005 – 2007': {
                position: "Senior Programmer",
                company: "Pisga Software & Communications Ltd.".location("Karmiel"),
                description: '''
                    Development of the Factory Management System for Windows platforms “xFactory”.
                ''',
                professional_skills: [
                    ('VB','C#','ASP.NET'), ('MSSQL','Oracle DB'),
                    ('XML','XSL'), ('HTML','DHTML'), ('JavaScript','AJAX','JSON')
                ]
            },
        '1998 – 2005': {
                position: "Leader Programmer",
                company: "S.A.E. Afikim".location("kibbuz Afikim"),
                description: '''
                    Development of the Computer-Assisted Dairy Management System “AfiFarm”.
                ''',
                professional_skills: [
                    ('C++','C','MFC'), ('Win16','Win32'), ('VXD','SYS','WDM'), 'TCP/IP',
                    ('ADO','DAO'), 'SQL'
                ]
            },
        '1996 – 1997': {
                position: "Leader Programmer",
                company: "Regional Interbank Clearing Centre".location("Khabarovsk, Russia"),
                description: '''
                    Development and management of the Interbank Payment System.
                ''',
                professional_skills: [
                    ('C++','C'), ('Unix','Linux'), 'TCP/IP', ('SQL','PLSQL')
                ]
            },
        '1994 – 1996': {
                position: "System Programmer",
                company: "Industrial-Building Bank of Russia".location("Khabarovsk, Russia"),
                description: '''
                    Development the Program System of Resource Management by the Booking
                    of Industrial-Building Bank of Russia.
                ''',
                professional_skills: [
                    ('C++','C'), ('DOS','Netware','Btrieve')
                ]
            }
    }

    EDUCATION = {
        '1988 – 1994': {
                Institution: "Khabarovsk State Technical University".location("Khabarovsk, Russia"),
                Degree: "M.A., Applied Mathematics"
            }
    }

    COURSES = {
        '2007': {
                Institution: "ECI Group".location("Petah-Tikwa"),
                Course: "Cellular Networking (2G, 3G, 4G)"
            },
        '2003': {
                Institution: "College “Hi-Tech”".location("Herzlia"),
                Course: "Building Distributed Applications with COM+ App"
            },
        '1999': {
                Institution: "College “Hi-Tech”".location("Herzlia"),
                Course: "Distributed Development Applications with DCOM, MTS, MSMQ and ADO"
            }
    }

    INTERESTS = '''
        My trade and profession has always been my strongest hobby. I love what I do,
        however, my interests are not limited only by my professional work. I like philosophical
        literature, progressive music, modern art, nature, people, and I especially love dogs :-)
    '''
