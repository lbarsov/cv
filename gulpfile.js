/*

  ==================================================
  ** Project : Gulpfile for Gulp-Compass-Neat
  ** Version : 0.0.1
  ** Author  : Vaibhav S
  ** Date    : 21.05.2015
  ==================================================

*/

var buildTime = new Date();

// Gulp Plugins
var gulp          = require('gulp'),
    debug         = require('gulp-debug'),
    uglify        = require('gulp-uglify'),
    gulpif           = require('gulp-if'),
    concat        = require('gulp-concat'),
    replace       = require('gulp-replace'),
    compass       = require('gulp-compass'),
    cssmin        = require('gulp-minify-css'),
    cssShorthand  = require('gulp-shorthand'),
    uncss         = require('gulp-uncss'),
    useref        = require('gulp-useref'),
    rename        = require('gulp-rename'),
    ignore        = require('gulp-ignore'), // Ignore files that don't need to be deleted from the folder
    rimraf        = require('gulp-rimraf'), // Cleaning CSS file generated
    gutil         = require('gulp-util'),   // Utility like beep when error
    plumber       = require('gulp-plumber'), // This is used so that the gulp watch doesn't break even though error
    imagemin      = require('gulp-imagemin'),
    datauri       = require('gulp-image-data-uri'),
    svgmin        = require('gulp-svgmin'),
    pngcrush      = require('imagemin-pngcrush'),
    htmlmin       = require('gulp-htmlmin'),
    notify        = require('gulp-notify'),
    browserSync   = require('browser-sync'),
    pdf           = require('html-pdf'),
    reload        = browserSync.reload,
    loadPlugins   = require('gulp-load-plugins'),
    plugins       = loadPlugins();

// Define path
var paths = {
    sass        : 'app/source/sass/**/*.scss',
    stylesheet  : 'app/source/sass',
    js          : 'app/source/js/**/*.js',
    img         : 'app/source/images/*',
    svg         : 'app/source/svg/*.svg',
    html        : 'app/*.html',
    pdf         : 'dist/*.html'
};

var dest  = {
    css         : 'dist/Css',
    script      : 'dist/Scripts/custom',
    image       : 'dist/Images',
    svg         : 'dist/Svg',
    html        : 'dist',
    pdf         : 'dist/Pdf'

};

// Compass Modules here
var modules = ['breakpoint'];

// Plumber Error
var onError = function (){
  gutil.beep();
  this.emit('end');
};

// Browser-Sync Server Start
gulp.task('browser-sync', function () {
  browserSync({
    port: 4000,
    notify: false,
      server: {
        baseDir : './app/',
     }
    });
});

// Notification Centre
var cleanUp       = "Cleanup Done";
var cssMessage    = "Compass Compilation Done";
var htmlMessage   = "HTML Changes Reloaded";
var imgMesssage   = "Images Compressed";
var jsMessage     = "Javascript Changes Reloaded";

/* ==========================

 ** TASKS BEGIN HERE

   ==========================
*/

// Compass Tasks here
gulp.task('compass', ['image', 'svg'], function() {
  return gulp
        .src(paths.sass)
        // .pipe(debug())
        .pipe(plumber({
            errorHandler: notify.onError("Compass build failed")
        }))
        .pipe(compass({
            sass        : paths.stylesheet,
            css         : dest.css,
            require     : modules
        }))
        .pipe(uncss({
            html: [dest.html + '/*.html']
        }))
        .pipe(cssShorthand())
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(dest.css))
        .pipe(notify({message: cssMessage}))
        .pipe(reload({stream: true})); // This is for Browser-Sync
});

// JS Tasks Here
gulp.task('scripts', function() {
  return gulp
      .src(paths.js)
      // .pipe(debug())
      .pipe(plumber({
          errorHandler: notify.onError("JS build failed")
      }))
      .pipe(concat('./compiled.min.js'))
      .pipe(uglify())
      .pipe(gulp.dest(dest.script))
      .pipe(notify({message: jsMessage}))
      .pipe(reload({stream: true})); // This is for Browser-Sync
});

// SVG Minfication Tasks Here
gulp.task('svg', function() {
  return gulp
    .src(paths.svg)
    // .pipe(debug())
    .pipe(svgmin())
    .pipe(gulp.dest(dest.svg))
    .pipe(notify({message: imgMesssage}))
    .pipe(reload({stream: true})); // This is for Browser-Sync
});

// Download QR code
gulp.task('qr', plugins.shell.task([
  "curl --url 'https://chart.googleapis.com/chart?cht=qr&chof=png&chf=bg,s,FFFFFF00|a,s,000020&chs=100x100&amp;chl=http://lbarsov.site/cv.html' -o app/source/images/cv.png",
  "curl --url 'https://chart.googleapis.com/chart?cht=qr&chof=png&chf=bg,s,FFFFFF00|a,s,000020&chs=100x100&amp;chl=http://lbarsov.site/cv.py.html' -o app/source/images/cv.py.png"
]));

// Image Minfication Tasks Here
gulp.task('image', ['qr'], function() {
  var condition = function (file) {
    var match = file.path.match(/cv(\.py)?\.png$/);
    return match !== null && match.length > 0;
  };

  return gulp
        .src(paths.img)
        .pipe(imagemin())
        .pipe(gulp.dest(dest.image))
        .pipe(debug())
        .pipe(gulpif(condition, datauri({template: {
              file: 'app/source/template2.tpl'
            }}),
            datauri({template: {
              file: 'app/source/template.tpl'
            }})
        ))
        .pipe(concat('inline-images.css'))
        .pipe(gulp.dest(dest.css))
        .pipe(uncss({
          html: [dest.html + '/*.html']
        }))
        .pipe(cssShorthand())
        .pipe(cssmin())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest(dest.css))
        .pipe(notify({message: imgMesssage}))
        .pipe(reload({stream: true})); // This is for Browser-Sync
});

// HTML Task Here
gulp.task('html', ['compass','scripts'], function() {
  return gulp
        .src(paths.html)
        .pipe(debug())
        .pipe(replace('{{ update-date }}', buildTime.toString()))
        .pipe(useref())
        .pipe(htmlmin({collapseWhitespace: true}))
        .pipe(gulp.dest(dest.html))
        .pipe(notify({message: htmlMessage}))
        .pipe(reload({stream: true})); // This is for Browser-Sync
});

// PDF Task here
gulp.task('pdf', ['html'], plugins.shell.task([
  'wkhtmltopdf --disable-javascript --margin-top 10mm  --margin-bottom 10mm dist/cv.html dist/Pdf/cv.pdf',
  'wkhtmltopdf --disable-javascript --margin-top 10mm  --margin-bottom 10mm dist/cv.py.html dist/Pdf/cv.py.pdf'
]));

// Clean up files that we don't need post build
gulp.task('clean', function() {
  return gulp
        .src(['app/Css/*.css', 'app/Scripts/*.js']) // Source of Folder to clean the files from
        .pipe(ignore(['*.min.css', '*.min.js'])) // Ignore files that don't need cleanup
        .pipe(rimraf()) // Actual clean up plugin
        .pipe(reload({stream: true})); // This is for Browser-Sync
});

//Watch here
gulp.task('watch', function() {
        gulp.watch(paths.sass, ['compass']);  // Compass Watch
        gulp.watch('app/css/*.css', ['clean']); // Clean Watch
        gulp.watch('app/*.html', ['html']); // HTML Watch
        gulp.watch(paths.js, ['scripts']); // Script Watch
        gulp.watch(paths.img, ['image']); // ImageMin Watch
});

gulp.task('build', ['clean','pdf']);

// Default Tasks
//gulp.task('default', ['watch', 'browser-sync']); // Default Task
gulp.task('default', ['build','watch','browser-sync']); // Default Task
